
import React from 'react'
import PropTypes from 'prop-types'
import GoogleMap from 'google-map-react'
import {data} from './data'
import Axios from 'axios';
import { Select } from 'antd';
import _ from 'lodash'
import 'antd/dist/antd.css';

const URL_BASE = 'http://3.1.205.229:4000/routing';
// const URL_BASE = 'http://localhost:4000/routing';
const {Option} = Select;

// const wayPoint1 = {
//   lat:"15.7744106",
//   lng:"107.7754650"
// };
//
// const daNangWayPoint = {
//   lat:"16.069340",
//   lng:"108.151616"
// };




export default class MapContainer extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      mapData: data,
      currentRoute: {},
      
    };
    this.paintedMarker = []
  }



  onGoogleApiLoaded=async ({ map, maps }) =>{
    this.map = map
    this.maps = maps

    this.renderDirectionRouter()
  };
  renderMapMarker = (waypts) => {
    const bounds = new this.maps.LatLngBounds()
    

    this.paintedMarker.map((item) => {
      item.setMap(null)
    })

    this.paintedMarker = []
    waypts.map((waypt,index) => {
      const tempMarker = new this.maps.Marker({
        position: waypt,
        map: this.map,
        icon: index === 0 ?'./pick.svg' : './icon.svg',
      })
      
      let title = ''
       _.split(waypt.title, ',').map((item,index)=>{title+= `<b class="mx-0 text-left font-weight-bold">${index!== 0 ? '- ':'<span style="margin-left: 10px;" />'}${item}</b><br/>`})
      tempMarker.addListener('click', () => {
        const contentString = `
        <div class="cursor-pointer">
        ${title}
        
        <div class="mx-0 mt-2 text-left row">
        </div>
        </div>
        `
        const tempInforWindow = new this.maps.InfoWindow({
          content: contentString,
     
          position: waypt,
        })
        this.paintedMarker.push(tempInforWindow)
        tempInforWindow.open(this.map)
      })
      // this.mapMarkerStart = { current: tempMarker, locationId: wayptPos.locationId }
      bounds.extend(tempMarker.position)

      this.paintedMarker.push(tempMarker)
      return null
    })
  }

  renderDirectionRouter = async (props)=> {
    if(!props) return
    if(JSON.stringify(props.truckRoute)=== JSON.stringify({})){
      return
    }
    const marker = []
    let path=[];
    if(props!=null){
       path = await this.spanRoad(props.truckRoute);
    }

    // const path=[];
    // path.push( {lat: parseFloat(currentRoute.pickUpLat), lng: parseFloat(currentRoute.pickUpLong)})
    marker.push( {lat: parseFloat(props.truckRoute.pickUpLat), lng: parseFloat(props.truckRoute.pickUpLong), title:props.truckRoute.pickUpCode })
    props.truckRoute.route.map((item)=>{
      // path.push( {lat: parseFloat(item.deliveryLat), lng: parseFloat(item.deliveryLong)})
      marker.push( {lat: parseFloat(item.deliveryLat), lng: parseFloat(item.deliveryLong), title:item.deliveryCode + ','+item.specDescription })
    })
    if (this.routePath) {
      this.routePath.setMap(null)
    }
    this.routePath = new this.maps.Polyline({
      path: path,
      geodesic: true,
      strokeColor: '#FF0000',
      strokeOpacity: 1.0,
      strokeWeight: 2
    })
    this.routePath.setMap(this.map)
    var bounds = new this.maps.LatLngBounds();
    if(path && path.length > 1){
      bounds.extend(path[0]);
      bounds.extend(path[path.length-1]);
    }
  
    this.map.fitBounds(bounds);
    this.renderMapMarker(marker)
  }

  /**
   * Doc json -> goi api Span road -> return danh sach diem
   * @param jsonObj
   * @return {Promise<Array>}
   */
   getRoutingFromGoogle = async ({ points, vehical = 'driving', format = 'json' }) => {
     const result = await Axios.post(URL_BASE, {
       points,
       vehical,
       format
   }
     )
      return result.data;
  };
  handleChange = (item)=>{
    this.setState({
      currentRoute: item,
     
    })
    this.renderDirectionRouter({
      truckRoute:item
    })
  }
  handleFileRead = (e) => {
    const content = this.fileReader.result;
   
    this.setState({
      mapData: JSON.parse(content),
    })
  };
  handleFileChosen = (file)=>{
    if(!file) return
    this.fileReader = new FileReader();
    this.fileReader.onloadend = this.handleFileRead;
    this.fileReader.readAsText(file);
  }

  isPointMienTay(point){
    return (point.lat<=15.7744106)
  }


  spanRoad=async (jsonObj)=>{
    let truckRoute = jsonObj;
    let pointList = [];
    let addDaNang = true;
    pointList.push({lat:truckRoute.pickUpLat,lng: truckRoute.pickUpLong});
    
    let routePoints = truckRoute.route;
    routePoints.forEach((point)=>{
      const temp= {lat:point.deliveryLat,lng: point.deliveryLong};
      pointList.push(temp);
    });

    const result = await this.getRoutingFromGoogle({points:pointList })
    if(result && result.data){
      const {  polyline } = result.data
     
      if (polyline) {
      const polylinePoints = this.maps.geometry.encoding.decodePath(polyline.points)
      return polylinePoints;
      }
    }
    return [];
  }

  render() {
    const {mapData={}} = this.state
    return (
      <div
        style={{ height: '100vh', width: '100wh' }}
      >
        <span style={{marginLeft: 20}}> Chọn route</span>
        <Select defaultValue='' style={{ width: 300, marginLeft: 10, marginTop: 20 }} onChange={(item)=>this.handleChange(JSON.parse(item))}>
            {(mapData.truckRoutes||[]).map((item,index)=>{
              return( 
                <Option key={index} value={JSON.stringify(item)}>{(index+1) + ' - ' + item.truck + ' - ' +  item.route.map((item,index)=>{
                  if(index===0) return item.deliveryCode
                  return ` - ${item.deliveryCode}`
                })}</Option>
             
              )
            })}          
          </Select>
        <span style={{marginLeft: 20}}> Upload file json</span>
        <input 
            style={{marginLeft: 20}}
            type='file'
            id='file'
            className='input-file'
            accept='.json'
            onChange={e => this.handleFileChosen(e.target.files[0])}
        />
        <div style={{ height: '100%', width: '100wh',  marginTop: 10}}>
          <GoogleMap
            bootstrapURLKeys={{
              key: 'AIzaSyD2lRESeQYU5Gg2O4LioWI_9YJ3XVx6k1U',
              language: 'en',
              region: 'vi',
              libraries: 'geometry',
            }}
            center={ { lat:12.136005,lng: 107.693422,}}
            onGoogleApiLoaded={this.onGoogleApiLoaded}
            yesIWantToUseGoogleMapApiInternals
            defaultZoom={1}
          >
       
          </GoogleMap>
         </div>
       
      </div>
    )
  }
}

MapContainer.propTypes = {
  originPoint: PropTypes.object,
  destinationPoint: PropTypes.object,
  arrayWayPoint: PropTypes.array,
  originAddress: PropTypes.string,
  destinationAddress: PropTypes.string,
  toEstimateTime: PropTypes.string,
  fromEstimateTime: PropTypes.string,
}

MapContainer.defaultProps = {
  originPoint: {},
  destinationPoint: {},
  arrayWayPoint: [],
  originAddress: '',
  destinationAddress: '',
  toEstimateTime: '',
  fromEstimateTime: '',
}

