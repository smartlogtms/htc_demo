const Point = require("./model/Point");

class MockPointUtil {

    static checkPair(from, to) {
        return this.isBac(from) && this.isNam(to);
    }

    /**
     * only add mock point to list if necessary
     * @param pointList
     * @return {Array}
     */
    static addMockPointToList(pointList) {
        let x;
        let result = [];
        for (x = 0; x < pointList.length; ++x) {
            if (x < pointList.length - 1) {
                const tempFrom = pointList[x];
                const tempTo = pointList[x + 1];
                if (this.checkPair(tempFrom, tempTo)) {
                    result.push(pointList[x]);
                    result.push(Point.DANANG);
                } else {
                    result.push(pointList[x]);
                }
            } else {
                result.push(pointList[x]);
            }
        }

        return result;
    }

    static isBac(point) {
        const bacThres = Point.create({
            lat: 16.484009,
            lng: 0
        });

        return point.isAbove(bacThres);
    }

    static isNam(point) {
        const namThres = Point.create({
            lat: 12.248511,
            lng: 107.438293
        });

        return point.isLeftOf(namThres) && point.isBelow(namThres);
    }
}


//test
// const from = Point.create({
//     lat: 20.9296638,
//     lng: 104.6607176
// });
//
// const to = Point.create({
//     lat: 10.844988,
//     lng: 105.705846
// });
//
// console.log(MockPointUtil.checkPair(from,to));
//
// const pointList = [
//     Point.create({
//         lat:20.3418709,
//         lng:105.9376756
//     }),
//     Point.create({
//         lat:10.008289,
//         lng:105.1120135
//     }),
//     Point.create({
//         lat:9.1706954,
//         lng:105.1468073
//     }),
// ];
//
//
// console.log(MockPointUtil.addMockPointToList(pointList).length===4);
// console.log(MockPointUtil.addMockPointToList(pointList)[1].equals(Point.DANANG));

module.exports = MockPointUtil;