const Point = require("./model/Point");
const MockPointUtil = require("./MockPointUtil");

class DistanceGetter {

    static create(googleMapClient){
        const getter = new DistanceGetter();
        getter.ggMapClient = googleMapClient;
        return getter;
    }

    async getDistanceMatrixGoogle ({fromArr, toArr}) {
        const apiResponse = await new Promise((resolve, reject) => {
            this.ggMapClient.distanceMatrix({
                    origins: fromArr,
                    destinations: toArr,
                    mode: "driving"
                },
                (err, response) => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve(response.json);
                    }
                })
        });

        return apiResponse;
    };

    async getDistanceGoogle({from,to}){
        const apiRes = await this.getDistanceMatrixGoogle({
            fromArr:[from],
            toArr:[to]
        });

        return apiRes.rows[0].elements[0].distance.value;
    }

    async getDistanceVN({from,to}){

        if(MockPointUtil.checkPair(from,to)){
            console.log("====get distance within vn===");
            const distance1 = await this.getDistanceGoogle({
                from:from,
                to:Point.DANANG
            });

            const distance2 = await this.getDistanceGoogle({
                from:Point.DANANG,
                to:to
            });

            return distance1+distance2;
        }else{
            console.log("none");
            return await this.getDistanceGoogle({
                from:from,
                to: to
            });
        }
    }
}

// // test code
// const googleMaps = require('@google/maps');
// const googleMapsClient = googleMaps.createClient({
//     key: 'AIzaSyAYRxmtFe-2vj-cgWUkdYHdiu3WePc4hLM',
// });
//
// const getter = DistanceGetter.create(googleMapsClient);
//
// getter.getDistanceVN({
//     from:Point.create({
//         lat:20.9296638,
//         lng:104.6607176
//     }),
//     to:Point.create({
//         lat:10.844988,
//         lng:105.705846
//     })
// }).then(res=>{
//     console.log(JSON.stringify(res))
// })


module.exports=DistanceGetter;