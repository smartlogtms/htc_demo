const express = require('express');
const bodyParser = require('body-parser');
const app = express();

app.use(require('cors')()); // enable CORS
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json({
}));
const port = process.env.PORT || 4000;
const server = require('http').Server(app);
const DistanceGetter = require("./DistanceGetter");
const Point = require("./model/Point");
const MockPointUtil = require("./MockPointUtil");
// boilerplate version
// const version = `Express-Boilerplate v${require('../package.json').version}`;

// start server
server.listen(port, () => {
  console.log(`Listening on port ${port}`);
});

const googleMaps = require('@google/maps')
const googleMapsClient = googleMaps.createClient({
  key: 'AIzaSyAYRxmtFe-2vj-cgWUkdYHdiu3WePc4hLM',
});

const getRoutingFromGoogle = async ({ points, vehical = 'driving', format = 'json' }) => {

  const routeResult = await new Promise((resolve, reject) => {
    googleMapsClient.directions(
      {
        origin: points[0],
        destination: points[points.length - 1],
        mode: vehical,
        optimize: true,
        waypoints: points.slice(1, -1),
        region: 'VN',
      },
      (err, response) => {
        if (err) {
          reject(err);
        } else {
          resolve(response.json);
        }
      },
    );
  });
  return routeResult;
}


const getDistanceGoogle = async ({point1, point2}) => {
    const ret = await new Promise((resolve, reject) => {
        googleMapsClient.distanceMatrix({
                origins: [point1],
                destinations: [point2],
                mode: "driving"
            },
            (err, response) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(response.json);
                }
            })
    });
    return ret;
};

// POST /login gets urlencoded bodies
app.post('/routing', async  (req, res) => {
  console.log("=====");
  const {body} = req
  console.log('body', body)
  if (!req.body.points) return res.sendStatus(400);

  const points = MockPointUtil.addMockPointToList(body.points.map(p => Point.create({lat: p.lat,lng:p.lng})));


  // const googleRoute = await getRoutingFromGoogle(body);
  const googleRoute = await getRoutingFromGoogle({points});
  const routes = googleRoute && googleRoute['routes'];
  // tslint:disable-next-line:no-string-literal
  const status = googleRoute && googleRoute['status'];
  if (status === 'OK' && routes && routes[0]) {
    const routeResult = { polyline: routes[0].overview_polyline };
    // return { data: routeResult };
    res.json({ data: routeResult })
  } else {
    res.json({ data: null})
  }
});

/**
 * body :{
 *     "from":{
 *         "lat":123,
 *         "lng":123
 *     },
 *     "to":{
 *         "lat":123,
 *         "lng":232
 *     }
 * }
 */
app.post("/distanceVn",async(request,response)=>{
    const {body}=request;
    if(!body) {
        return response.sendStatus(400);
    }

    console.log(JSON.stringify(body));

    const from = Point.create(body.from);
    const to = Point.create(body.to);

    const distance = await DistanceGetter.create(googleMapsClient)
        .getDistanceVN({
            from:from,
            to:to
        });

    response.json({
        distance:distance
    })
});