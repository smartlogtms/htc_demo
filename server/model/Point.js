class Point {

    constructor(){
        this.lat=0;
        this.lng=0;
    }

    static create({lat,lng}){
        const p = new Point();
        p.lat = lat;
        p.lng = lng;
        return p;
    }

    static get DANANG(){
        return Point.create({
            lat:16.069340,
            lng:108.151616
        })
    }

    isLeftOf(anotherPoint){
        return this.lng <= anotherPoint.lng;
    }

    isRightOf(anotherPoint){
        return !this.isLeftOf(anotherPoint);
    }

    isAbove(anotherPoint){
        return this.lat >= anotherPoint.lat;
    }

    isBelow(anotherPoint){
        return !this.isAbove(anotherPoint);
    }

    equals(anotherPoint){
        return this.lat === anotherPoint.lat && this.lng === anotherPoint.lng;
    }
}

module.exports = Point;